// import React from 'react';
// import { StyleSheet,
//          Text,
//          View, 
//          ImageBackground,
//          ScrollView,
//        } from 'react-native';

// import { Icon } from 'react-native-elements'


// export default function App (props) {




// //==================== HANDLE ACTIONS ==========================

//   const handlePress = () => {
//     //props.history.push('/')
//   }



// ///////////////////////////  PAGE STARTS  ///////////////////////////////////
// ////////////////////////////////////////////////////////////////////////////


//     return (
//         <ImageBackground  source={{uri: 'https://images.unsplash.com/photo-1549490349-8643362247b5?ixlib=rb-1.2.1&w=1000&q=80'}}
//                         style={styles.container}>
//                 <View style={styles.header}>
//                     <Text style={styles.heading}>OptionZ</Text>
//                 </View>

//                 <ScrollView style={styles.messagebox}>
//                     <View>
//                         <TouchableOpacity onPress={()=>handlePress()}>
//                                 <Icon
//                                 reverse
//                                 name='times-circle'
//                                 type='font-awesome'
//                                 color='black'
//                                 size={15}
//                         />
//                         </TouchableOpacity>
//                     </View>

//                     <Text>Welcome to OptionZ</Text>
//                     <Text>OptionZ is an app to help you make a decision when faced with too many options to choose from </Text>

//                 </ScrollView>

//                 <TouchableOpacity style={styles.customButton}
//                                   onPress={()=>handlePress()}>
//                     <Text style={styles.buttonText}>Got it</Text>
//                 </TouchableOpacity>

//         </ImageBackground>
//     );
// }

// //////////////////////////  STYLES  //////////////////////////////////////////


// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },

//   header: {
//     marginTop:30,
//     width: '100%',
//     height: '15%',
//     //borderWidth: 1,
//     alignItems: 'center',
//     justifyContent: 'center',
//     flexDirection: 'row',
//   },
//   // text: {
//   //   fontSize: 40
//   // },
//   messageBox: {
//     width: '100%',
//     height: '15%',
//     borderBottomWidth: 1,
//     borderBottomColor: 'black',
//     alignItems: 'center',
//     justifyContent: 'center',
//     flexDirection: 'row',
//   },
// //   text: {
// //     fontSize: 25,
// //     fontWeight: 'bold',
// //     letterSpacing: 1,
// //     color: 'white',//'#61c191',
// //     textShadowRadius: 100,
// //     textShadowColor: 'black',
// //   },
//   heading: {
//     fontSize: 60,
//     fontWeight: 'bold',
//     color: 'black',
//     fontFamily: 'sans-serif-condensed',
//     textShadowRadius: 40,
//     textShadowColor: 'white',
//   },

//   buttonStyle: {
//     backgroundColor: '#61c191'
//   },

//   customButton: {
//     height: 40,
//     width: 100,
//     backgroundColor: 'black',
//     marginLeft: 5,
//     borderRadius: 5,
//     padding: 10,
//   },

//   buttonText: {
//     color: 'white',
//     textAlign: 'center',
//     fontWeight: 'bold',
//     letterSpacing: 0.5
//   }

// });