import React, { useState, useEffect} from 'react';
import {Button, Text, StyleSheet, View, ImageBackground, TouchableOpacity} from 'react-native';
import FadeInOut from 'react-native-fade-in-out';
import { ShakeEventExpo } from './components/shake';


 
export default function App() {

    useEffect(()=>{
        shakeOn()
    },[])

    const [ visible, setVisible ] = useState(true)
    const [chosen, setChosen] = useState('')

    const options = [
        'one',
        'two',
        'three',
        'four',
        'five',
        'six'
    ];

  
 
  const toggleVisible = () => {
    if (visible){
        setVisible(!visible)
    } else {
        setVisible(visible)
    }
  }

  const shakeOn = async () => {
   
    ShakeEventExpo.addListener(() => {
      //add your code here
      console.log('Shake Shake Shake');
        let choices = options
        let temp = choices[Math.floor(Math.random()*choices.length)];
        console.log(temp)
        if (options.length <= 1){
            setChosen('Haha Nice try!!')
        } else {
        setChosen(temp)
        }
        shakeOff()
    });
  }

  const shakeOff = () => {
    ShakeEventExpo.removeListener();
    toggleVisible()
  }

  const handleDone = () => {
      setVisible(!visible)
      console.log ('not visible')
  }

 
    return (
      <ImageBackground  source={{uri: 'https://images.unsplash.com/photo-1549490349-8643362247b5?ixlib=rb-1.2.1&w=1000&q=80'}}
                        style={styles.container}>

            <View style={styles.list}>
            
                {

                    options.map((ele,i)=>{
                        return <View key={i} style={styles.singleOption}>
                                    <FadeInOut style={styles.singleOption} visible={visible} scale={true} duration={1000} rotate={true}>
                                        <Text style={styles.text}>
                                            {ele}
                                        </Text>                                
                                    </FadeInOut>
                        </View>
                    })
                }

        
            {/* <Button onPress={toggleVisible} title="Start Fade" /> */}

            </View>

            <View style={styles.container}>
                    <FadeInOut style={styles.answerBox} visible={!visible} scale={true} duration={2000} rotate={true}>
                        <View>
                            <Text style={styles.answerBoxText}>{chosen}</Text>
                            <TouchableOpacity style={styles.answerButton}
                                   onPress={()=>handleDone()}
                            >
                                <Text style={styles.answerButtonText}>DONE</Text>
                            </TouchableOpacity>
                        </View>  


                    </FadeInOut>

            </View>

      </ImageBackground>
    );
  }
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },

  singleOption: {
    width: '100%',
    height: 60,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: 5,
    marginLeft:5,
    paddingRight: 10 
  },
  list: {
    width: '100%',
    height: '20%',

  },
  text: {
    fontSize: 25,
    fontWeight: 'bold',
    letterSpacing: 1,
    color: 'white',//'#61c191',
    textShadowRadius: 100,
    textShadowColor: 'black',
  },


  answerBox: {
    justifyContent: 'center',
    marginLeft:'10%',
    width: '80%',
    height: 200,
    backgroundColor: 'black',
    borderColor: 'white',
    borderWidth: 3,
    borderRadius:100
  },

  answerBoxText: {
    fontSize: 25,
    fontWeight: 'bold',
    letterSpacing: 1,
    color: 'white',//'#61c191',
    textAlign: 'center'
  },

  answerButton: {
    height: 40,
    width: 80,
    backgroundColor: 'white',
    paddingTop:8,
    marginLeft: 100,
    marginTop:10,
    borderRadius: 100,
    borderColor: 'purple',
    borderWidth: 3
  },

  answerButtonText: {
    color: 'black',
    fontWeight: 'bold',
    letterSpacing: 0.5,
    textAlign: 'center',
    fontFamily: 'sans-serif-condensed'
  }



});




//=================OLDEST VERSION ===================================

// import React, {useState, useEffect} from 'react';
// //import Welcome from './components/welcome';
// import { ShakeEventExpo } from './components/shake';
// import FadeInOut from 'react-native-fade-in-out';
// import Constants from 'expo-constants';

// import { StyleSheet,
//          Text,
//          View, 
//          TextInput, 
//          //Button, 
//          TouchableOpacity,
//          AsyncStorage,
//          Alert,
//          ImageBackground,
//          ScrollView,
//        } from 'react-native';

// import { Icon } from 'react-native-elements' // make sure you install in your project from the terminal => sudo npm install react-native-elements


// export default function App() {

//   const [option, setOption] = useState ('')
//   const [options, setOptions] = useState ([])
//   const [chosen, setChosen] = useState ('')
//   const [updated, setUpdated] = useState ('')
//   const [visible, setVisible ] = useState(true)


//   useEffect( ()=>{
//     findOptions()
//     shakeOn()
//   },[])

//   //============== FadeInOut Section==================

//   const toggleVisible = () => {
//    if (visible){
//      setVisible(!visible)
//      } else {
//        setVisible(visible)
//      }
//   }

      




//   const shakeOn = async () => {
//     const value = await AsyncStorage.getItem('options');
//     const parsed = JSON.parse(value)
//     ShakeEventExpo.addListener(() => {
//       //add your code here
//       console.log('Shake Shake Shake');
//       console.log(parsed);
//         let choices = parsed
//         let temp = choices[Math.floor(Math.random()*parsed.length)];
//         console.log(temp)
//         if (parsed.length <= 1){
//           setChosen('Haha Nice try!!')
//         } else {
//         setChosen(temp)
//         }
//       shakeOff()
//     });
//   }

//   const shakeOff = () => {
//     ShakeEventExpo.removeListener();
 
//     toggleVisible()
  
    
//   }

// //==================== FIND TODOS ==========================

//   const findOptions = async () => {
//     try{
//       const value = await AsyncStorage.getItem('options');
//       if (value === null) {
//         await AsyncStorage.setItem('options', JSON.stringify([]))
//         console.log(value);
//       }else{
//         const parsed = JSON.parse(value)
//         setOptions([...parsed])
//       }
//     } catch (err){
//         console.log('SetOptions is wrong!')
//         handleError()
//     }
//   }


// //==================== HANDLE ACTIONS ==========================

//   const handlePress = async () => {
//     try{
//       if (options.length === 6){
//         Alert.alert(
//           'Sorry',
//           'You can only have 6 options',
//           [
//           {text: 'ok', onPress: () => console.log('Ok pressed')},
//           ],{cancelable: false}
//         )
//         setOption('')
//       } else if (options.length < 6) {
//           const value = await AsyncStorage.getItem('options');
//           const parsed = JSON.parse(value)
//           // debugger
//           let temp = parsed
//           temp.push(option)
//           await AsyncStorage.setItem('options', JSON.stringify(temp))
//           setOptions([...temp])
//           setOption('')
//     }
       
//     } catch (err){
//         console.log('handlePress is wrong!',err)
//         handleError()
//     }   
//   }

//   const handleError = () => {
//     Alert.alert(
//       'Error',
//       'something is wrong!',
//       [
//       {text: 'ok', onPress: () => console.log('Ok pressed')},
//       ],{cancelable: false}
//     )

//   }

//   const handleDone = () => {
//     setVisible(!visible)
//     console.log ('not visible')
//   }

// //==================== DELETE TODOS ==========================


//   const confirmRemove = (i) => {
//     Alert.alert(
//       'Remove Option',
//       'Are you sure?',
//     [
//       {text: 'cancel', onPress: () => console.log('cancel pressed'), style: 'cancel'},
//       {text: 'ok', onPress: ()=> removeOption(i)},
//     ], {cancelable: false}
//     )
//   }


//   const removeOption = async (i) => {
//     try{
//       const value = await AsyncStorage.getItem('options');
//       const parsed = JSON.parse(value)
//       //debugger
//       let temp = parsed
//       temp.splice(i,1)  
//       await AsyncStorage.setItem('options', JSON.stringify(temp))
//       setOptions([...temp])       
//     } catch (err){
//         console.log('something is wrong!',err)
//     }
//   }

//   const editOption = async (i, updated) => {
//     try{
//       const value = await AsyncStorage.getItem('options');
//       const parsed = JSON.parse(value)
//       //debugger
//       let temp = parsed
//       temp[i]= updated  
//       await AsyncStorage.setItem('options', JSON.stringify(temp))
//       setOptions([...temp])       
//     } catch (err){
//         console.log('edit Option is wrong!',err)
//     }
//   }

// //====================== CLEAR / RESET OPTIONS ==============================


// // const clearOptions = async () => {
// //   try{
// //     const value = await AsyncStorage.getItem('options');
// //     const parsed = JSON.parse(value)
// //     //debugger
// //     let temp = parsed
// //     temp = []
// //     await AsyncStorage.setItem('options', JSON.stringify(temp))
// //     setOptions([...temp])       
// //   } catch (err){
// //       console.log('clearOptions is wrong!',err)
// //   }
// // }


// ///////////////////////////  PAGE STARTS  ///////////////////////////////////
// ////////////////////////////////////////////////////////////////////////////


//   return (
//     <ImageBackground  source={{uri: 'https://images.unsplash.com/photo-1549490349-8643362247b5?ixlib=rb-1.2.1&w=1000&q=80'}}
//                       style={styles.container}>


//         <View style={styles.header}
//               visible={true}
//         >
//           <Text style={styles.heading}>OptionZ</Text>
//         </View>

//         <View style={styles.form}>
//           <TextInput style={styles.input}
//                      value={option}
//                      placeholder={'enter your option here '}
//                      onChangeText={text => setOption(text)}/>
          
//           <TouchableOpacity style={styles.customButton}
//                   onPress={()=>handlePress()}>
//               <Text style={styles.buttonText}>Add Option</Text>
//           </TouchableOpacity>

//         </View>

//         <View style={styles.list}>
//           {
//             options.map((ele, i)=>{
//               return <View key={i} style={styles.singleOption}>
//                        <FadeInOut style={styles.singleOption} visible={visible} scale={true} duration={1000} rotate={true}>
//                           <TextInput editable={true}
//                                     onChangeText={text => setUpdated(text)}
//                                     style={styles.text}
//                                     onSubmitEditing={()=> editOption(i, updated)}
                                    
//                           >
//                           {ele}
//                           </TextInput>
                    
//                           <TouchableOpacity onPress={()=>confirmRemove(i)}>
//                               <Icon
//                                 reverse
//                                 name='times-circle'
//                                 type='font-awesome'
//                                 color='black'
//                                 size={15}
//                               />
//                           </TouchableOpacity>
//                         </FadeInOut>
//                      </View>
                     
//             })
//           }
//         </View>


//         <View style={styles.list}>
//                     <FadeInOut style={styles.answerBox} visible={!visible} scale={true} duration={2000} rotate={true}>
                      
//                             <Text style={styles.answerBoxText}>{chosen}</Text>
//                             <TouchableOpacity style={styles.answerButton}
//                                    onPress={()=>handleDone()}
//                             >
//                                 <Text style={styles.answerButtonText}>DONE</Text>
//                             </TouchableOpacity>
                      
//                     </FadeInOut>
//         </View>


//     </ImageBackground>

//   );
// }


// //////////////////////////  STYLES  //////////////////////////////////////////


// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     marginTop: Constants.statusBarHeight,
//   },
//   header: {
//     width: '100%',
//     height: '20%',
//     alignItems: 'center',
//     justifyContent: 'center',
//     flexDirection: 'row',
//   },

//   form: {
//     width: '100%',
//     height: '10%',
//     alignItems: 'center',
//     justifyContent: 'center',
//     flexDirection: 'row',
//     backgroundColor: 'red',
//   },

//   list: {
//     width: '100%',
//     height: '70%',
//     alignItems: 'center',
//     justifyContent: 'center',
//     //backgroundColor: '#fff',
//   },
//   input: {
//     width: '65%',
//     height: 40,
//     borderWidth: 2,
//     borderColor: 'black',
//     borderRadius: 5,
//     paddingLeft: 5,
//     fontSize: 20,
//     color: 'black',
//     backgroundColor: 'white'
    
//   },
//   singleOption: {
//     width: '100%',
//     height: 60,
//     flexDirection: 'row',
//     alignItems: 'center',
//     justifyContent: 'space-between',
//     paddingLeft: 5,
//     marginLeft:5,
//     paddingRight: 10 
//   },
//   text: {
//     fontSize: 25,
//     fontWeight: 'bold',
//     letterSpacing: 1,
//     color: 'white',
//     textShadowRadius: 100,
//     textShadowColor: 'black',
//   },
//   heading: {
//     fontSize: 60,
//     fontWeight: 'bold',
//     color: 'black',
//     fontFamily: 'sans-serif-condensed',
//     textShadowRadius: 40,
//     textShadowColor: 'white',
//   },
//   add: {
//     backgroundColor: 'black',
//     color: 'green'
//   },
//   buttonStyle: {
//     backgroundColor: '#61c191'
//   },

//   customButton: {
//     height: 40,
//     width: 100,
//     backgroundColor: 'black',
//     marginLeft: 5,
//     borderRadius: 5,
//     padding: 10,
//   },

//   buttonText: {
//     color: 'white',
//     textAlign: 'center',
//     fontWeight: 'bold',
//     letterSpacing: 0.5
//   },

//   answerBox: {
//     justifyContent: 'center',
//     //marginBottom: '150%',
//     width: '80%',
//     height: 200,
//     backgroundColor: 'blue',
//     borderColor: 'white',
//     borderWidth: 3,
//     borderRadius:100,
//     alignItems: 'center',
//     flexDirection: 'column'
    
//   },

//   answerBoxText: {
//     fontSize: 25,
//     fontWeight: 'bold',
//     letterSpacing: 1,
//     color: 'white',//'#61c191',
//     textAlign: 'center'
//   },

//   answerButton: {
//     height: 40,
//     width: 80,
//     backgroundColor: 'white',
//     //paddingTop:8,
//     alignItems: 'center',
//     justifyContent: 'center',
//     // marginLeft: 30,
//     marginTop:10,
//     borderRadius: 100,
//     borderColor: 'purple',
//     borderWidth: 3
//   },

//   answerButtonText: {
//     color: 'black',
//     fontWeight: 'bold',
//     letterSpacing: 0.5,
//     textAlign: 'center',
//     fontFamily: 'sans-serif-condensed'
//   },

// });




//++++++++++++++++++ OLD VERSION +++++++++++++++++++++++++++++++++++++++


import React, {useState, useEffect} from 'react';
//import Welcome from './components/welcome';
import { ShakeEventExpo } from './components/shake';
import FadeInOut from 'react-native-fade-in-out';
import Constants from 'expo-constants';

import { StyleSheet,
         Text,
         View, 
         TextInput, 
         //Button, 
         TouchableOpacity,
         AsyncStorage,
         Alert,
         ImageBackground,
         ScrollView,
       } from 'react-native';

import { Icon } from 'react-native-elements' // make sure you install in your project from the terminal => sudo npm install react-native-elements


export default function App() {

  const [option, setOption] = useState ('')
  const [options, setOptions] = useState ([])
  const [chosen, setChosen] = useState ('')
  const [updated, setUpdated] = useState ('')
  const [visible, setVisible ] = useState(true)


  useEffect( ()=>{
    findOptions()
    shakeOn()
  },[])

  //============== FadeInOut Section==================

  const toggleVisible = () => {
   if (visible){
     setVisible(!visible)
     } else {
       setVisible(visible)
     }
  }

      




  const shakeOn = async () => {
    const value = await AsyncStorage.getItem('options');
    const parsed = JSON.parse(value)
    ShakeEventExpo.addListener(() => {
      //add your code here
      console.log('Shake Shake Shake');
      console.log(parsed);
        let choices = parsed
        let temp = choices[Math.floor(Math.random()*parsed.length)];
        console.log(temp)
        if (parsed.length <= 1){
          setChosen('Haha Nice try!!')
        } else {
        setChosen(temp)
        }
      shakeOff()
    });
  }

  const shakeOff = () => {
    ShakeEventExpo.removeListener();
 
    toggleVisible()
  
    
  }

//==================== FIND TODOS ==========================

  const findOptions = async () => {
    try{
      const value = await AsyncStorage.getItem('options');
      if (value === null) {
        await AsyncStorage.setItem('options', JSON.stringify([]))
        console.log(value);
      }else{
        const parsed = JSON.parse(value)
        setOptions([...parsed])
      }
    } catch (err){
        console.log('SetOptions is wrong!')
        handleError()
    }
  }


//==================== HANDLE ACTIONS ==========================

  const handlePress = async () => {
    try{
      if (options.length === 6){
        Alert.alert(
          'Sorry',
          'You can only have 6 options',
          [
          {text: 'ok', onPress: () => console.log('Ok pressed')},
          ],{cancelable: false}
        )
        setOption('')
      } else if (options.length < 6) {
          const value = await AsyncStorage.getItem('options');
          const parsed = JSON.parse(value)
          // debugger
          let temp = parsed
          temp.push(option)
          await AsyncStorage.setItem('options', JSON.stringify(temp))
          setOptions([...temp])
          setOption('')
    }
       
    } catch (err){
        console.log('handlePress is wrong!',err)
        handleError()
    }   
  }

  const handleError = () => {
    Alert.alert(
      'Error',
      'something is wrong!',
      [
      {text: 'ok', onPress: () => console.log('Ok pressed')},
      ],{cancelable: false}
    )

  }

  const handleDone = () => {
    setVisible(!visible)
    console.log ('not visible')
  }

//==================== DELETE TODOS ==========================


  const confirmRemove = (i) => {
    Alert.alert(
      'Remove Option',
      'Are you sure?',
    [
      {text: 'cancel', onPress: () => console.log('cancel pressed'), style: 'cancel'},
      {text: 'ok', onPress: ()=> removeOption(i)},
    ], {cancelable: false}
    )
  }


  const removeOption = async (i) => {
    try{
      const value = await AsyncStorage.getItem('options');
      const parsed = JSON.parse(value)
      //debugger
      let temp = parsed
      temp.splice(i,1)  
      await AsyncStorage.setItem('options', JSON.stringify(temp))
      setOptions([...temp])       
    } catch (err){
        console.log('something is wrong!',err)
    }
  }

  const editOption = async (i, updated) => {
    try{
      const value = await AsyncStorage.getItem('options');
      const parsed = JSON.parse(value)
      //debugger
      let temp = parsed
      temp[i]= updated  
      await AsyncStorage.setItem('options', JSON.stringify(temp))
      setOptions([...temp])       
    } catch (err){
        console.log('edit Option is wrong!',err)
    }
  }

//====================== CLEAR / RESET OPTIONS ==============================


const clearOptions = async () => {
    try{
      const value = await AsyncStorage.getItem('options');
      const parsed = JSON.parse(value)
      //debugger
      let temp = parsed
      temp = []
      await AsyncStorage.setItem('options', JSON.stringify(temp))
      setOptions([...temp])       
    } catch (err){
        console.log('clearOptions is wrong!',err)
    }
  }


///////////////////////////  PAGE STARTS  ///////////////////////////////////
////////////////////////////////////////////////////////////////////////////


  return (
    <ImageBackground  source={{uri: 'https://images.unsplash.com/photo-1549490349-8643362247b5?ixlib=rb-1.2.1&w=1000&q=80'}}
                      style={styles.container}>


        <View style={styles.header}
              visible={true}
        >
          <Text style={styles.heading}>OptionZ</Text>
        </View>

        <View style={styles.form}>
          <TextInput style={styles.input}
                     value={option}
                     placeholder={'enter your option here '}
                     onChangeText={text => setOption(text)}/>
          
          <TouchableOpacity style={styles.customButton}
                  onPress={()=>handlePress()}>
              <Text style={styles.buttonText}>Add Option</Text>
          </TouchableOpacity>

        </View>


        <View style = {styles.resetContainer}>
            <TouchableOpacity onPress={()=>clearOptions()}>   
                <Text style = {styles.resetContainerText}>Reset</Text>
            </TouchableOpacity>
        </View>

      
       
         <View style={styles.list}>
          {
            options.map((ele, i)=>{
              return <View key={i} style={styles.singleOption}>
                       <FadeInOut style={styles.singleOption} visible={visible} scale={true} duration={1000} rotate={true}>
                          <TextInput editable={true}
                                    onChangeText={text => setUpdated(text)}
                                    style={styles.text}
                                    onSubmitEditing={()=> editOption(i, updated)}
                                    
                          >
                          {ele}
                          </TextInput>
                    
                          <TouchableOpacity onPress={()=>confirmRemove(i)}>
                              <Icon
                                reverse
                                name='times-circle'
                                type='font-awesome'
                                color='black'
                                size={15}
                              />
                          </TouchableOpacity>
                        </FadeInOut>
                     </View>
                     
            })
          }
        </View>


       <View style={styles.list}>
                    <FadeInOut style={styles.answerBox} visible={!visible} scale={true} duration={2000} rotate={true}>
                      
                            <Text style={styles.answerBoxText}>{chosen}</Text>
                            <TouchableOpacity style={styles.answerButton}
                                   onPress={()=>handleDone()}
                            >
                                <Text style={styles.answerButtonText}>DONE</Text>
                            </TouchableOpacity>
                      
                    </FadeInOut>
        </View>

    
    </ImageBackground>

  );
}


//////////////////////////  STYLES  //////////////////////////////////////////


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop: Constants.statusBarHeight,
  },
  header: {
    width: '100%',
    height: '20%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },

  form: {
    width: '100%',
    height: '10%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    // backgroundColor: 'red',
  },

  resetContainer: {
    width: '100%',
    height: '5%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: 'orange'
    
  },

  resetContainerText: {
    fontWeight: 'bold',
    fontFamily: 'sans-serif-condensed',
    fontSize: 20,
    textDecorationLine: 'underline',
    textShadowRadius: 40,
    textShadowColor: 'white',
  },

  list: {
    width: '100%',
    height: '65%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  input: {
    width: '65%',
    height: 40,
    borderWidth: 2,
    borderColor: 'black',
    borderRadius: 5,
    paddingLeft: 5,
    fontSize: 20,
    color: 'black',
    backgroundColor: 'white'
    
  },
  singleOption: {
    width: '100%',
    height: 60,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: 5,
    // marginLeft:5,
    paddingRight: 10 ,
    backgroundColor: 'yellow'
  },
  text: {
    fontSize: 25,
    fontWeight: 'bold',
    letterSpacing: 1,
    color: 'white',
    textShadowRadius: 100,
    textShadowColor: 'black',
  },
  heading: {
    fontSize: 60,
    fontWeight: 'bold',
    color: 'black',
    fontFamily: 'sans-serif-condensed',
    textShadowRadius: 40,
    textShadowColor: 'white',
  },
  add: {
    backgroundColor: 'black',
    color: 'green'
  },
  buttonStyle: {
    backgroundColor: '#61c191'
  },

  customButton: {
    height: 40,
    width: 100,
    backgroundColor: 'black',
    marginLeft: 5,
    borderRadius: 5,
    padding: 10,
  },

  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
    letterSpacing: 0.5
  },

  answerBox: {
    justifyContent: 'center',
    //marginBottom: '150%',
    width: '80%',
    height: 200,
    backgroundColor: 'blue',
    borderColor: 'white',
    borderWidth: 3,
    borderRadius:100,
    alignItems: 'center',
    flexDirection: 'column'
    
  },

  answerBoxText: {
    fontSize: 25,
    fontWeight: 'bold',
    letterSpacing: 1,
    color: 'white',//'#61c191',
    textAlign: 'center'
  },

  answerButton: {
    height: 40,
    width: 80,
    backgroundColor: 'white',
    //paddingTop:8,
    alignItems: 'center',
    justifyContent: 'center',
    // marginLeft: 30,
    marginTop:10,
    borderRadius: 100,
    borderColor: 'purple',
    borderWidth: 3
  },

  answerButtonText: {
    color: 'black',
    fontWeight: 'bold',
    letterSpacing: 0.5,
    textAlign: 'center',
    fontFamily: 'sans-serif-condensed'
  },

});




