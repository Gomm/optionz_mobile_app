import React, { useState, useEffect } from 'react'
import FadeInOut from 'react-native-fade-in-out';
import Constants from 'expo-constants';

import { StyleSheet,
         Text,
         View, 
         TextInput, 
         //Button, 
         TouchableOpacity,
         AsyncStorage,
         Alert,
         ImageBackground,
         ScrollView,
       } from 'react-native';

       import { Icon } from 'react-native-elements' // make sure you install in your project from the terminal => sudo npm install react-native-elements

const List = props => { 
return <View style={styles.list}>
    {
      props.options.map((ele, i)=>{
        return <View key={i} style={styles.singleOption}>
                 <FadeInOut style={styles.singleOption} visible={props.visible} scale={true} duration={2000} >
                    <TextInput editable={true}
                              onChangeText={text => props.setUpdated(text)}
                              style={styles.text}
                              onSubmitEditing={()=> props.editOption(i, props.updated)}
                              
                    >
                    {ele}
                    </TextInput>
              
                    <TouchableOpacity onPress={()=> props.confirmRemove(i)}>
                        <Icon
                          reverse
                          name='times-circle'
                          type='font-awesome'
                          color='black'
                          size={15}
                        />
                    </TouchableOpacity>
                  </FadeInOut>
               </View>
               
      })
    }
  </View> 
}

const Answer = props => {
    const [display, setDisplay ] = useState(false)
    useEffect( ()=>{
        setDisplay(true)
      },[])
    return <View style={styles.answer}>
        <FadeInOut style={styles.answerBox} visible={display} scale={true} duration={1000} rotate={true}>
        
                <Text style={styles.answerBoxText}>{props.chosen}</Text>
                <TouchableOpacity style={styles.answerButton}
                    onPress={()=> props.handleDone()}
                >
                    <Text style={styles.answerButtonText}>DONE</Text>
                </TouchableOpacity>
        
        </FadeInOut>
   </View>
}

export { List, Answer }


//////////////////////////  STYLES  //////////////////////////////////////////


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      marginTop: Constants.statusBarHeight,
    },
    header: {
      width: '100%',
      height: '20%',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'row',
    },
  
    form: {
      width: '100%',
      height: '10%',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'row',
      // backgroundColor: 'red',
    },
  
    resetContainer: {
      width: '100%',
      height: '5%',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'row',
    //   backgroundColor: 'orange'
      
    },
  
    resetContainerText: {
      fontWeight: 'bold',
      fontFamily: 'sans-serif-condensed',
      fontSize: 20,
      textDecorationLine: 'underline',
      textShadowRadius: 40,
      textShadowColor: 'white',
    },
  
    list: {
      width: '100%',
      height: '65%',
      alignItems: 'center',
    //   backgroundColor: '#fff',
    },

    answer: {
        width: '100%',
        height: '65%',
        alignItems: 'center',
        justifyContent: 'center'
    },

    input: {
      width: '65%',
      height: 40,
      borderWidth: 2,
      borderColor: 'black',
      borderRadius: 5,
      paddingLeft: 5,
      fontSize: 20,
      color: 'black',
      backgroundColor: 'white'
      
    },
    singleOption: {
      width: '100%',
      height: 60,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingLeft: 5,
      // marginLeft:5,
      paddingRight: 10 ,
    //   backgroundColor: 'yellow'
    },
    text: {
      fontSize: 25,
      fontWeight: 'bold',
      letterSpacing: 1,
      color: 'white',
      textShadowRadius: 100,
      textShadowColor: 'black',
    },
    heading: {
      fontSize: 60,
      fontWeight: 'bold',
      color: 'black',
      fontFamily: 'sans-serif-condensed',
      textShadowRadius: 40,
      textShadowColor: 'white',
    },
    add: {
      backgroundColor: 'black',
      color: 'green'
    },
    buttonStyle: {
      backgroundColor: '#61c191'
    },
  
    customButton: {
      height: 40,
      width: 100,
      backgroundColor: 'black',
      marginLeft: 5,
      borderRadius: 5,
      padding: 10,
    },
  
    buttonText: {
      color: 'white',
      textAlign: 'center',
      fontWeight: 'bold',
      letterSpacing: 0.5
    },
  
    answerBox: {
      justifyContent: 'center',
      //marginBottom: '150%',
      width: '80%',
      height: 200,
      backgroundColor: 'black',
      borderColor: 'white',
      borderWidth: 3,
      borderRadius:100,
      alignItems: 'center',
      flexDirection: 'column'
      
    },
  
    answerBoxText: {
      fontSize: 25,
      fontWeight: 'bold',
      letterSpacing: 1,
      color: 'white',//'#61c191',
      textAlign: 'center'
    },
  
    answerButton: {
      height: 40,
      width: 80,
      backgroundColor: 'white',
      //paddingTop:8,
      alignItems: 'center',
      justifyContent: 'center',
      // marginLeft: 30,
      marginTop:10,
      borderRadius: 100,
      borderColor: 'purple',
      borderWidth: 3
    },
    answerButtonText: {
      color: 'black',
      fontWeight: 'bold',
      letterSpacing: 0.5,
      textAlign: 'center',
      fontFamily: 'sans-serif-condensed'
    },
  
  });
  